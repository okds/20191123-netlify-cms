---
templateKey: index-page
title: TITLE
image: /img/img_9353_compressed.jpg
heading: HEADING
subheading: SUBHEADIG
mainpitch:
  title: MAINPICH TITLE
  description: description
description: HEADINGのdescription
intro:
  blurbs:
    - image: /img/coffee.png
      text: ここはBLURBS　１つめ
    - image: /img/meeting-space.png
      text: ここはBLURBS　２つめ
  heading: INTRO　HEADING
  description: description
main:
  heading: MAIN HEADING
  description: MAIN HEADING description
  image1:
    alt: A close-up of a paper filter filled with ground coffee
    image: /img/products-grid3.jpg
  image2:
    alt: A green cup of a coffee on a wooden table
    image: /img/products-grid2.jpg
  image3:
    alt: Coffee beans
    image: /img/products-grid1.jpg
---

