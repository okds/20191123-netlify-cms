---
templateKey: blog-post
title: スラッグを自分で決められるようにする
date: 2019-11-24T03:16:20.829Z
description: スラッグを自分で決められるようにする方法
featuredimage: /img/image-4-.webp
tags:
  - howto
image: /img/image-4-.webp
slug: how-to-slug
---
## step1
```　
collections:
- name: "blog"
```
にある

slugを次のように変更する

```
slug: "{{fields.slug}}"
```

## step2

fields: 　の任意の場所に次のコードを挿入。わからなければとりあえず最後にいれる。

```
- {label: "Slug", name: "slug", widget: "string"}
```

これを追加することで、「SLUG」という入力項目が記事作成画面に追加され、そこに入れた任意のスラッグがURLに反映されるようになる。
